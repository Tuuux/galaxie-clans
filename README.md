# Galaxie - Clans

> Every gamer needs a clan.
> Find here everything a gamer [clan](https://en.wikipedia.org/wiki/Clan) needs to live in the [Information Age](https://en.wikipedia.org/wiki/Information_Age).

* **Who is it for?**: Litteraly every group of friend willing to communicate over Internet.
* **Why do this?**:
  * Because we can.
  * Because Open-Source is cool.
  * For the fun.

### Workspace prerequisites

* [Debian](https://www.debian.org/) or [Alpine](https://www.alpinelinux.org/) system
* Make
* Your user in sudoers (system package manager at least)
* a text editor

## Once upon a time...

Once upon a time, this project was hosted on a ancient platform called GitHub. Then came the Buyer. The Buyer bought GitHub, willing to rule over its community. We were not to sell, so here is the new home of "https://github.com/Tuuux/galaxie", and the project has mutated since then...

We build an Ansible toolkit for managing a set of Debian servers, a set of powerful roles made hand-in-hand by a senior system admin and a senior developer.

Originaly written for managing a Home Network with external services. The project has shifted to managing digital survival of clanic entities.

Galaxie-Clans is a true mix of the two world, by luck author Tuuux and Mo have made the necessary to have no compromise.

Bear in mind folks: Tech purity is cancer for the mind. Efficiency and usability matter.

It would be an honor if you could use our work in any way. Be it as-is or as code samples.

Have fun, hack in peace.
