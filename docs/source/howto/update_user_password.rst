*****************************************
Update a user's password
*****************************************

====================
Ready?
====================

The target host must be in the ``galaxie-clans``.

====================
Set.
====================

* ``$INVENTORY_HOSTNAME`` is the name of the target host
* ``$UNAME`` should match the ``uname`` of a user managed by ``system_users`` role

====================
Go!
====================

Get password hash
--------------------

The user must, on his side, use this command to hash the password he likes:

.. code:: bash

    mkpasswd -m SHA-512

...and give it to the clan caretaker. 

.. note::

    Until the end, this user-supplied hash value will be referenced as `$UHASH`.

Store password hash
--------------------

Run:

.. code:: bash

    ansible-playbook playbooks/ops_store_user_hash.yml \
        -e scope=$INVENTORY_HOSTNAME \
        -e uname=$UNAME \
        -e uhash='$UHASH'


.. warning::

    Beware of the single quotes around `$UHASH`.


Spread password hash value
--------------------------

Run:

.. code:: bash

    ansible-playbook playbooks/setup_core_services.yml -e scope=$INVENTORY_HOSTNAME

If the clan host has some of broadcast services enabled, also run:

.. code:: bash

    ansible-playbook playbooks/setup_broadcast_services.yml -e scope=$INVENTORY_HOSTNAME 

