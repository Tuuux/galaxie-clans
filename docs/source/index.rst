*************
Galaxie-Clans
*************

This project is an opinionated Ansible collection.

    Every gamer needs a `clan <https://en.wikipedia.org/wiki/Clan>`_.

    Find here everything a gamer clan needs to live in the `Information Age <https://en.wikipedia.org/wiki/Information_Age>`_.

* **Who is it for?**

  * Litteraly every group of friends willing to communicate over Internet.

* **Why even bother to do this?**

  * Because we can.
  * Because Open-Source is cool.
  * For the fun.

Now :doc:`/howto/get_started`!

Lost?
=====

.. toctree::
   :maxdepth: 1

   tutorials
   howto
   explanation
   reference

This `documentation structure <https://documentation.divio.com/>`_ is borrowed from Daniele Procida's presentation at PyCon Australia: "What nobody tells you about documentation".

Blessed be he for his teachings.
