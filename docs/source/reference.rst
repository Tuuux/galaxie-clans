*******************************
Reference
*******************************

.. toctree::
   :maxdepth: 1

   reference/role_system_base
   reference/role_calendar
   reference/role_chat
   reference/role_dns
   reference/role_firewall
   reference/role_k3s
   reference/role_mailserver
   reference/role_monitor
   reference/role_peertube
   reference/role_rancher
   reference/role_rproxy
   reference/role_system_users
   reference/role_taiga
   reference/role_videoconf
