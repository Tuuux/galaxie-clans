*****************
Role: system_base
*****************

Will setup everything a host need to live a true life: ntp, locales,
system and pip packages, etc.
Is intented to be the very first role to apply to any host, in order
to have the basic tooling.

Variables & Defaults
====================

Hostname
--------

Defaults to a variation of ``inventory_hostname`` because
underscores are not valid characters in host names.
::

  system_base_hostname: "{{ inventory_hostname | replace('_','-') }}"


Locale
------

::

  system_base_primary_locale: 'fr_FR.UTF-8'
  system_base_secondary_locale: 'en_US.UTF-8'
  system_base_language: 'fr'


Expected system packages
------------------------

::

  system_base_system_packages:
    - "acl"
    - "aptitude"
    - "apt-transport-https"
    - "bash"
    - "bash-completion"
    - "build-essential"
    - "cpufrequtils"
    - "cron"
    - "curl"
    - "debian-archive-keyring"
    - "direnv"
    - "dnsutils"
    - "dphys-swapfile"
    - "figlet"
    - "findutils"
    - "git"
    - "glances"
    - "gnupg"
    - "grc"
    - "haveged"
    - "htop"
    - "libffi-dev"
    - "libperl-dev"
    - "libssl-dev"
    - "locales"
    - "mailutils"
    - "mc"
    - "minicom"
    - "ntp"
    - "openssl"
    - "powertop"
    - "procps"
    - "python3"
    - "python3-apt"
    - "python3-distutils-extra"
    - "python3-pip"
    - "python3-setuptools"
    - "python3-venv"
    - "python3-wheel"
    - "rng-tools5"
    - "rsync"
    - "screen"
    - "ssl-cert"
    - "sudo"
    - "task-spooler"
    - "tzdata"
    - "vim"
    - "vim-common"
    - "wget"


Unwanted system packages
------------------------

::

  system_base_unwanted_system_packages:
    - "exim4"
    - "exim4-base"
    - "exim4-config"
    - "exim4-daemon-heavy"
    - "exim4-daemon-light"
    - "exim4-dev"
    - "exim4-doc-html"
    - "exim4-doc-info"
    - "eximon4"
    - "nano"
    - "ntpdate"
    - "sa-exim"
    - "vim-tiny"


Expected Python packages
------------------------

::

  system_base_pip_packages:
    - "dnspython"
    - "cryptography"
    - "passlib"
    - "bcrypt"
    - "ansible"


Sysctl configuration
--------------------

::

  system_base_sysctl_vm_swappiness: "10"
  system_base_sysctl_vm_vfs_cache_pressure: "50"
  system_base_sysctl_vm_overcommit_memory: "0"
  system_base_sysctl_vm_overcommit_ratio: "95"
  system_base_sysctl_net_core_wmem_max: "1048576"
  system_base_sysctl_net_core_rmem_max: "10485760"


Vim configuration
-----------------

::

  system_base_vimrc_lines:
    - regexp: >-
        .*syntax
      line: "syntax on"
    - regexp: >-
        .*set background=[a-zA-Z0-9_]+
      line: "set background=dark"
    - regexp: >-
        .*set tabstop=[0-9]+
      line: set tabstop=4
    - regexp: >-
        .*set shiftwidth=[0-9]+
      line: set shiftwidth=4
    - regexp: >-
        .*set expandtab
      line: set expandtab
    - regexp: >-
        .*set noautoindent
      line: set noautoindent
    - regexp: >-
        .*set mouse=[a-zA-Z0-9_]+
      line: set mouse=
    - regexp: >-
        .*set ttymouse=[a-zA-Z0-9_]+
      line: set ttymouse=


Banner and Message Of The Day
-----------------------------

::

  system_base_banner_text: "Galaxie Clans"
  system_base_domain: "{{ ansible_hostname | default('clans.galaxie.family') }}"


Private certificate store
-------------------------

The role will create a system user group 'ssl-cert' and only users belonging
to this will have access to the content of the ``{{ system_base_ssl_certs_dir }}`` directory.
::

  system_base_ssl_certs_dir: "/etc/ssl/private"


Time configuration
------------------

::

  system_base_timezone: "Europe/Paris"
  system_base_ntp_server_upper_stratum:
    - "0.pool.ntp.org"
    - "1.pool.ntp.org"
    - "2.pool.ntp.org"
    - "3.pool.ntp.org"
  system_base_ntp_restrict:
    - "127.0.0.1"
    - "::1"


``moleculed``
-------------
Set this to ``True`` only when you are testing with Molecule
in a container environment.
::

  moleculed: False
