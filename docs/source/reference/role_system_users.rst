******************
Role: system_users
******************

Handles users and users groups creation

Variables & Defaults
====================


- gname:
  gid:
  ::

    system_users_groups: []

    system_users: []

- uname: "zorro"
  uid: "42001"
  groups:
    - "sudo"
  home: "/home/zorro"
  shell: "/bin/bash"
  authorized_key: "ssh-ed25519 ..."

