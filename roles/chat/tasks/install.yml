---
- name: Create mattermost group
  group:
    name: "{{ mattermost_user.group }}"
    gid: "{{ mattermost_user.gid }}"

- name: Create mattermost user
  user:
    name: "{{ mattermost_user.name }}"
    uid: "{{ mattermost_user.uid }}"
    home: "{{ mattermost_user.home }}"
    create_home: "{{ mattermost_user.create_home | default(omit) }}"
    group: "{{ mattermost_user.group }}"
    groups: "{{ mattermost_user.groups | default(omit) }}"
    shell: "{{ mattermost_user.shell | default('/bin/false') }}"

- name: Create mattermost database group
  group:
    name: "{{ mattermost_db_user.group }}"
    gid: "{{ mattermost_db_user.gid }}"

- name: Create mattermost database user
  user:
    name: "{{ mattermost_db_user.name }}"
    uid: "{{ mattermost_db_user.uid }}"
    home: "{{ mattermost_db_user.home }}"
    create_home: "{{ mattermost_db_user.create_home | default(omit) }}"
    group: "{{ mattermost_db_user.group }}"
    groups: "{{ mattermost_db_user.groups | default(omit) }}"
    shell: "{{ mattermost_db_user.shell | default('/bin/false') }}"

- name: Prepare directories
  file:
    path: "{{ item }}"
    state: directory
    owner: "{{ mattermost_user.name }}"
    group: "{{ mattermost_user.group }}"
    mode: 0750
    recurse: yes
  loop: "{{ mattermost_directories }}"

- name: Prepare database directories
  file:
    path: "{{ item }}"
    state: directory
    owner: "{{ mattermost_db_user.name }}"
    group: "{{ mattermost_db_user.group }}"
    mode: "u=rwX,g=rX"
    recurse: yes
  loop: "{{ mattermost_db_directories }}"

- name: Get jitsi plugin
  get_url:
    url: https://github.com/mattermost/mattermost-plugin-jitsi/releases/download/v2.0.0/jitsi-2.0.0.tar.gz
    dest: "{{ mattermost_plugins_directory }}/jitsi-2.0.0.tar.gz"

- name: Expand jitsi plugin
  unarchive:
    src: "{{ mattermost_plugins_directory }}/jitsi-2.0.0.tar.gz"
    dest: "{{ mattermost_plugins_directory }}/"
    remote_src: yes

- name: Render docker-compose file
  template:
    src: "docker-compose.yml.j2"
    dest: "{{ mattermost_docker_compose_file }}"
    owner: "{{ mattermost_user.name }}"
    group: "{{ mattermost_user.group }}"
    mode: 0640
  notify:
    - Restart mattermost service

- name: Render config file
  template:
    src: "config.json.j2"
    dest: "{{ mattermost_config_directory }}/config.json"
    owner: "{{ mattermost_user.name }}"
    group: "{{ mattermost_user.group }}"
    mode: 0640
  notify:
    - Restart mattermost service

- name: Render docker-compose environment file
  template:
    src: "docker-compose.env.j2"
    dest: "{{ mattermost_docker_compose_env_file }}"
    owner: "{{ mattermost_user.name }}"
    group: "{{ mattermost_user.group }}"
    mode: 0640
  notify:
    - Restart mattermost service

- name: Render systemd service
  template:
    src: "systemd.service.j2"
    dest: "{{ mattermost_systemd_service_file }}"
  register: _mattermost_systemd_service_render
  notify:
    - Restart mattermost service

- name: Render nginx site
  template:
    src: "nginx.conf.j2"
    dest: "/etc/nginx/sites-available/mattermost.conf"

- name: Pre-pull images
  docker_image:
    name: "{{ item.value }}"
    source: pull
  loop: "{{ mattermost_docker_images | dict2items }}"

- name: Render backup profile
  template:
    src: "backup_profile.j2"
    dest: "{{ chat_backup_profile_file }}"
  notify: Reload local facts
    
- name: Start service
  systemd:
    name: mattermost
    state: started
    daemon_reload: "{{ _mattermost_systemd_service_render is changed }}"

- meta: flush_handlers