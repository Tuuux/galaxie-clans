---
- name: Install bind
  package:
    pkg: "{{ bind_packages }}"
    state: present

- name: Ensure runtime directories referenced in config exist
  file:
    path: "{{ item }}"
    state: directory
    owner: root
    group: "{{ bind_group }}"
    setype: named_cache_t
    mode: 0770
  loop: "{{ bind_directories }}"

- name: Create serial, based on UTC UNIX time
  command: >-
    date -u +%s
  register: timestamp
  changed_when: false
  run_once: true
  check_mode: false

- include_tasks: "_data_collect.yml"

- name: Master | Main BIND config file (master)
  template:
    src: master_etc_named.conf.j2
    dest: "{{ bind_config }}"
    owner: "{{ bind_owner }}"
    group: "{{ bind_group }}"
    mode: '0640'
    setype: named_conf_t
    validate: 'named-checkconf %s'
  notify: Restart bind

- name: Master | Create forward lookup zone file
  template:
    src: bind_zone.j2
    dest: "{{ bind_zone_dir }}/{{ item.name }}"
    owner: "{{ bind_owner }}"
    group: "{{ bind_group }}"
    mode: "{{ bind_zone_file_mode }}"
    setype: named_zone_t
    validate: 'named-checkzone -d {{ item.name }} %s'
  loop: "{{ bind_master_zone_domains }}"
  loop_control:
    label: "{{ item.name }}"
  when: bind_zones_updates[item.name].forward_update
  notify: Restart bind

- name: Master | Create reverse lookup zone file
  template:
    src: reverse_zone.j2
    dest: "{{ bind_zone_dir }}/{{ ('.'.join(item.1.replace(item.1+'.','').split('.')[::-1])) }}.in-addr.arpa"
    owner: "{{ bind_owner }}"
    group: "{{ bind_group }}"
    mode: "{{ bind_zone_file_mode }}"
    setype: named_zone_t
    validate: "named-checkzone {{ ('.'.join(item.1.replace(item.1+'.','').split('.')[::-1])) }}.in-addr.arpa %s"
  loop: "{{ lookup('subelements', bind_master_zone_domains, 'networks', {'skip_missing': True}) }}"
  loop_control:
    label: "{{ item.1 }}"
  when: bind_zones_updates[item.name].reverse_update
  notify: Restart bind

- name: Master | Create reverse IPv6 lookup zone file
  template:
    src: reverse_zone_ipv6.j2
    dest: "{{ bind_zone_dir }}/{{ (item.1 | ipaddr('revdns'))[-(9+(item.1|regex_replace('^.*/','')|int)//2):-1] }}"
    owner: "{{ bind_owner }}"
    group: "{{ bind_group }}"
    mode: "{{ bind_zone_file_mode }}"
    setype: named_zone_t
    validate: "named-checkzone {{ (item.1 | ipaddr('revdns'))[-(9+(item.1|regex_replace('^.*/','')|int)//2):] }} %s"
  loop: "{{ lookup('subelements', bind_master_zone_domains, 'ipv6_networks', {'skip_missing': True}) }}"
  loop_control:
    label: "{{ item.1 }}"
  when: bind_zones_updates[item.name].reverse_ipv6_update
  notify: Restart bind

# file to set keys for XFR authentication
- name: create extra config file for authenticated XFR request
  template:
    src: auth_transfer.j2
    dest: "{{ bind_conf_dir }}/{{ auth_file }}"
    mode: 0640
    owner: root
    group: "{{ bind_group }}"
  when: bind_dns_keys is defined and bind_dns_keys|length > 0
  notify: Restart bind

- include_tasks: "_data_store.yml"

- name: Start BIND service
  service:
    name: "{{ bind_service }}"
    state: started
    enabled: true

- meta: flush_handlers
