---
- name: "Opendkim - {{ _opendkim_domain }} - Sanity checks"
  assert:
    that:
      - _opendkim_domain is defined

- name: "Opendkim - {{ _opendkim_domain }} - Cooking variables"
  set_fact:
    _opendkim_domain_directory: "{{ opendkim_key_directory }}/{{ _opendkim_domain }}"

- name: "Opendkim - {{ _opendkim_domain }} - delete dkim key to force rotation"
  shell: >-
    rm -f {{ opendkim_key_directory }}/{{ _opendkim_domain }}/mail.private
    {{ opendkim_key_directory }}/{{ _opendkim_domain }}/mail.txt
  when: opendkim_rotate_key is defined and opendkim_rotate_key | bool

- name: "Opendkim - {{ _opendkim_domain }} - generate key"
  shell: >-
    opendkim-genkey -b {{ opendkim_key_size }} -D {{ _opendkim_domain_directory }} -d {{ _opendkim_domain }} -s mail
    && chown opendkim: {{ opendkim_key_directory }} -R
  args:
    creates: "{{ _opendkim_domain_directory }}/mail.private"

- name: "Opendkim - {{ _opendkim_domain }} - render key table"
  lineinfile:
    dest: "{{ opendkim_key_table_file }}"
    line: >-
      mail._domainkey.{{ _opendkim_domain }} {{ _opendkim_domain }}:mail:{{ _opendkim_domain_directory }}/mail.private
    owner: opendkim
    group: opendkim
    create: yes
  notify: Restart opendkim service

- name: "Opendkim - {{ _opendkim_domain }} - render signing table"
  lineinfile:
    dest: "{{ opendkim_signing_table_file }}"
    line: >-
      {{ _opendkim_domain }} mail._domainkey.{{ _opendkim_domain }}
    owner: opendkim
    group: opendkim
    create: yes
  notify: Restart opendkim service

- name: "Opendkim - {{ _opendkim_domain }} - render trusted hosts table"
  lineinfile:
    dest: "{{ opendkim_trusted_hosts_file }}"
    line: >-
      {{ _opendkim_domain }}
    owner: opendkim
    group: opendkim
    create: yes
  notify: Restart opendkim service

- name: "Opendkim - {{ _opendkim_domain }} - find out what the remote machine's mounts are"
  slurp:
    src: "{{ _opendkim_domain_directory }}/mail.txt"
  register: _opendkim_domain_key

- name: "Opendkim - {{ _opendkim_domain }} - extract key"
  set_fact:
    _opendkim_domain_full_record: >-
      {{
        _opendkim_domain_key['content'] | b64decode
        | regex_replace('.*\( ', '')
        | regex_replace(' \) .*', '')
        | regex_replace('[\n\t\(\)\" ]', '')
      }}

- name: "Opendkim - {{ _opendkim_domain }} - Create record 'mail._domainkey'"
  nsupdate:
    server: "127.0.0.1"
    zone: "{{ _opendkim_domain }}"
    record: "mail._domainkey"
    type: TXT
    value:
      - "{{ _opendkim_domain_full_record }}"
    state: present
  register: _domainkey_record_creation
  when:
    - render_opendkim_record is not defined or not render_opendkim_record

- name: Render opendkim record formatted for mailserver role integration
  debug:
    msg: |-
      # insert in zone {{ _opendkim_domain }}
      - name: "mail._domainkey"
        text: "{{ _opendkim_domain_full_record }}"
  when:
    - render_opendkim_record is defined
    - render_opendkim_record | bool


- name: "Opendkim - {{ _opendkim_domain }} - sync record with zone file"
  shell: >-
    rndc sync -clean {{ _opendkim_domain }}
  when:
    - _domainkey_record_creation is defined
    - _domainkey_record_creation is changed
